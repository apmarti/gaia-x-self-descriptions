/**
 * Als JS-File geschrieben, damit man schönes Syntax-Highlighting hat
 * und Kommentare schreiben kann :)
 * 
 * Erster grober Entwurf, wie die Beschreibung eines Hardware-Knotens aussehen
 * könnte. Properties wie z.B. `ram.capacity`, `cpu.cores`, etc. könnten streng
 * getypt oder als einfache Key-Value-Pairs umgesetzt werden.
 * 
 * Die bestehenden System zur Beschreibung von Hardware-Knoten sind recht
 * generisch. So bietet z.B. http://vocab.linkeddata.es/wicus/hwspecs
 * die Konzepte von `Memory`, `CPU`, `Network` und `Storage`. Diesen Komponenten
 * wird dann aber lediglich eine Liste an Features (Name + Wert + Einheit) zugeordnet.
 * 
 * Im Feld `sourcing` unterscheiden wir aktuell in `manufacturer` und `vendor`.
 * Das ist sehr stark vereinfacht und wird der komplexen Realität von Lieferketten
 * natürlich nicht gerecht. Was ist mit Auftragsfertigern? Was ist mit Produkten,
 * die während der Produktion den Produktionsort wechseln? Was ist mit Anteilseignern,
 * die aus einem anderen Land als der Hersteller stammt? Wo kommen überhaupt all die
 * Chips her, die auf dem Mainboard verlötet sind? Etc...
 * 
 * Von der eigentlichen Anforderung wäre damit erst der Hardware-Teil
 * abgedeckt.
 * 
 *   Umsetzung eines Proof-of-Concepts zur Selbstbeschreibung eines Managed-Kubernetes-Angebots
 *   mittels GAIA-X-Beschreibungsformat unter Einbeziehung des kompletten Software- und
 *   Hardware-Stacks inklusive Informationen zur Hardwarefertigung
 */

hardwareNode = {
    model: {
        name: "X 123/4",
        version: "Ver.1.0",
        variant: "Y",
        serialNumber: "123443234",
    },
    sourcing: {
        manufacturer: {
            name: "Thomas Krenn",
            jurisdiction: "Germany",
        },
        vendor: {
            name: "Thomas Krenn",
            jurisdiction: "Germany",
        },
    },
    log: [
        {
            // TODO: In die Form bringen, dass es zu Metadata, Testimonials, Proof passt
            event: "assembly-completed",
            timestamp: "2020-10-09T07:30:00+0000",
            signedOf: {
                entity: "peter.müller@vendor",
                signature: "480329049320940329409"
            }
        },
        {
            event: "prepared-for-shipping",
            timestamp: "2020-10-09T08:40:00+0000",
            signedOf: {
                entity: "søren.peters@vendor",
                signature: "498732948732948732974"
            }
        },
        {
            event: "picked-up-for-shipping",
            timestamp: "2020-10-09T10:00:00+0000",
            signedOf: {
                entity: "driver@dhl",
                signature: "9237493274987324932"
            }
        },
        {
            event: "delivered-to-recipient",
            timestamp: "2020-10-10T08:00:00+0000",
            signedOf: {
                entity: "luisa.baum@datacenter",
                signature: "4823794327493429"
            }
        },
        {
            event: "testing-concluded",
            timestamp: "2020-10-10T12:00:00+0000",
            signedOf: {
                entity: "sahra.wagner@datacenter",
                signature: "5092843095843095843"
            }
        },
        {
            event: "physically-installed",
            timestamp: "2020-10-10T13:00:00+0000",
            signedOf: {
                entity: "kai.bross@datacenter",
                signature: "40923749873294873294"
            }
        },
        {
            event: "moved-to-production",
            timestamp: "2020-10-10T17:00:00+0000",
            signedOf: {
                entity: "gabriel.enzmann@datacenter",
                signature: "9874323204938420023"
            }
        },
    ],
    components: {
        bios: {
            type: "uefi",
            opensource: false,
            license: "proprietary",
            sourcing: {
                manufacturer: {
                    name: "AMIBIOS",
                    jurisdiction: "USA",
                },
            },
        },
        managementEngine: {
            name: "AMD Secure Technology"
        },
        remoteMangement: {
            name: "IPMI 2.0"
        },
        ram: {
            capacity: 16384,
            ecc: true,
            modules: [
                {
                    capacity: 8192,
                    sourcing: {
                        manufacturer: {
                            name: "Infineon",
                            jurisdiction: "Germany",
                        },
                        vendor: {
                            name: "Zulieferer GmbH",
                            jurisdiction: "Germany",
                        },
                    }
                },
                {
                    capacity: 8192,
                    sourcing: {
                        manufacturer: {
                            name: "Infineon",
                            jurisdiction: "Germany",
                        },
                        vendor: {
                            name: "Zulieferer GmbH",
                            jurisdiction: "Germany",
                        },
                    }
                },
            ]
        },
        cpus: [
            {
                model: "AMD EPYC",
                cores: 8,
                threads: 16,
                minMHz: 3700,
                maxMHz: 3900,
                cache: 128000,
                tdp: 180,
                sourcing: {
                    manufacturer: {
                        name: "AMD",
                        jurisdiction: "USA",
                    },
                    vendor: {
                        name: "Zulieferer GmbH",
                        jurisdiction: "Germany",
                    },
                }
            },
            {
                model: "AMD EPYC",
                cores: 8,
                threads: 16,
                minMHz: 3700,
                maxMHz: 3900,
                cache: 128000,
                tdp: 180,
                sourcing: {
                    manufacturer: {
                        name: "AMD",
                        jurisdiction: "USA",
                    },
                    vendor: {
                        name: "Zulieferer GmbH",
                        jurisdiction: "Germany",
                    },
                }
            },
        ],
        storage: {
            totalCapacity: 500000,
            drives: [
                {
                    model: "D3-S4510",
                    type: "SSD",
                    interface: "NVM",
                    readSpeed: 560,
                    writeSpeed: 280,
                    readIops: 90000,
                    writeIops: 16000,
                    sourcing: {
                        manufacturer: {
                            name: "Intel",
                            jurisdiction: "USA",
                        },
                        vendor: {
                            name: "Some Company",
                            jurisdiction: "Poland",
                        },
                    }
                }
            ]
        },
        mainboard: {
            model: "Supermicro Mainboard X11DPi-N",
            sourcing: {
                manufacturer: {
                    name: "Supermicro",
                    jurisdiction: "USA",
                },
                vendor: {
                    name: "Other Company",
                    jurisdiction: "France",
                },
            }
        },
        network: {
            nics: [
                {
                    model: "Intel i210AT",
                    dataRate: "1000",
                    sourcing: {
                        manufacturer: {
                            name: "Intel",
                            jurisdiction: "USA",
                        },
                        vendor: {
                            name: "Other Company",
                            jurisdiction: "France",
                        },
                    }
                }
            ]
        },
        energy: {
            powerSupplies: [
                {
                    manufacturer: "AIC",
                    hotswap: true,
                    power: 550,
                    efficency: 94,
                    sourcing: {
                        manufacturer: {
                            name: "AIC",
                            jurisdiction: "Taiwan",
                        },
                        vendor: {
                            name: "Other Company",
                            jurisdiction: "France",
                        },
                    }
                }
            ]
        },
    },
    cooling: {
        type: "water",
        sourcing: {
            manufacturer: {
                name: "Cloud & Heat",
                jurisdiction: "Germany",
            },
            vendor: {
                name: "Cloud & Heat",
                jurisdiction: "Germany",
            }
        }
    },
}

